import React from 'react';
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts';
import highchartsMap from 'highcharts/modules/map';
import mapDataCA from './mapDataCA.json';
import Spinner from 'react-bootstrap/Spinner';
import _ from 'lodash';
import { connect } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import { fetchProvDetails } from './common/actions';

highchartsMap(Highcharts); // Initialize the map module

const provinceCodes = {
  Canada: 'ca-5682',
  'British Columbia': 'ca-bc',
  Nunavut: 'ca-nu',
  'Northwest Territories': 'ca-nt',
  Alberta: 'ca-ab',
  'Newfoundland and Labrador': 'ca-nl',
  Saskatchewan: 'ca-sk',
  Manitoba: 'ca-mb',
  Quebec: 'ca-qc',
  Ontario: 'ca-on',
  'New Brunswick': 'ca-nb',
  'Nova Scotia': 'ca-ns',
  'Prince Edward Island': 'ca-pe',
  Yukon: 'ca-yt',
};

const mapOptions = {
  chart: {
    map: 'countries/ca/ca-all',
  },

  title: {
    text: "COVID-19 cases across Canada's provinces",
  },

  subtitle: {
    text:
      'Source map: <a href="http://code.highcharts.com/mapdata/countries/ca/ca-all.js">Canada</a>',
  },

  mapNavigation: {
    enabled: true,
    buttonOptions: {
      verticalAlign: 'bottom',
    },
  },

  colorAxis: {
    min: 0,
  },
};
class Map extends React.Component {
  componentDidMount() {
    const { getProvDetails } = this.props;
    getProvDetails();
  }
  render() {
    const { provDetails } = this.props;

    if (provDetails.isLoading) {
      return <Spinner animation="border" variant="warning" />;
    }
    if (provDetails.hasError) {
      return <div>There was an error with the server</div>;
    }
    if (provDetails.payload) {
      const { data } = provDetails.payload;

      const provincialData = _.groupBy(data, 'prname');

      const highMapsData = Object.entries(provincialData)
        .filter((entry) => entry[0] !== 'Repatriated travellers')
        .map((entry) => {
          return [
            provinceCodes[entry[0]],
            entry[1].reduce((result, next) => {
              return result + Number(next.numtoday);
            }, 0),
          ];
        });

      mapOptions.series = [
        {
          name: 'Basemap',
          mapData: mapDataCA,
          borderColor: '#A0A0A0',
          nullColor: 'rgba(200, 200, 200, 0.3)',
          showInLegend: false,
          data: highMapsData,
          states: {
            hover: {
              color: '#BADA55',
            },
          },
          dataLabels: {
            enabled: true,
            format: '{point.name}',
          },
        },
      ];
      return (
        <div>
          <HighchartsReact
            constructorType={'mapChart'}
            highcharts={Highcharts}
            options={mapOptions}
          />
        </div>
      );
    }
    return null;
  }
}

const mapState = ({ app: { provDetails } }) => ({
  provDetails,
});

const mapDispatch = {
  getProvDetails: fetchProvDetails,
};

export default connect(mapState, mapDispatch)(Map);

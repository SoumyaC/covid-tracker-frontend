import React from 'react';
import { hot } from 'react-hot-loader/root';
import Map from './map';

class App extends React.Component {
  render() {
    return (
      <div>
        <Map />
      </div>
    );
  }
}

export default hot(App);

import { fetchProvinceDetailsType } from '../actions';
import { getInitState } from '../utils/async-middleware';

const initialState = {
  provDetails: getInitState(),
};

export default (state = { ...initialState }, action) => {
  switch (action.type) {
    case fetchProvinceDetailsType:
      return { ...state, provDetails: { ...state.provDetails, ...action } };

    default:
      return state;
  }
};

import axios from 'axios';
export const fetchProvinceDetailsType = 'FETCH_PROVINCE_DETAILS';

export const fetchProvDetails = () => (dispatch) => {
  const PROVINCE_ID_URL = `http://localhost:5000/covid19data`;
  return dispatch({
    type: fetchProvinceDetailsType,
    payload: axios.get(PROVINCE_ID_URL),
  });
};
